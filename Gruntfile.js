module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
		Bower : {
			path : 'bower_components/'
		},
		Cake : {
			// webroot
			path_webroot : 'app/webroot/',
			// js
			path_js : '<%= Cake.path_webroot %>js/',
			// css
			path_css : '<%= Cake.path_webroot %>css/'
		},
		pkg : grunt.file.readJSON('package.json'),
		bower : {
			install : {
				options : {
					targetDir : '<%= Cake.path_webroot %>vendor',
					layout : 'byComponent',
					install : true,
					verbose : true,
					cleanTargetDir : true,
					cleanBowerDir : false
				}
			}
		},
		compass : {
			// Cake
			cake : {
				options : {
					config : 'compass_config.rb'
				}
			}
		},
		cssmin : {
			// Cake
			cake : {
				src : '<%= Cake.path_css %>src/*.css',
				dest : '<%= Cake.path_css %>build/main.min.css'
			}
		},
		uglify : {
			options : {
				mangle : false
			},
			cake : {
				src : '<%= Cake.path_js %>src/*.js',
				dest : '<%= Cake.path_js %>build/main.min.js'
			}
		},
		jshint : {
			files : [ 'Gruntfile.js', '<%= Cake.path_js %>src/*.js' ],
			options : {
				globals : {
					jQuery : true
				}
			}
		}
    });
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-bower-task');
	
	grunt.registerTask('default', ['cssmin','uglify']);
};
