<?php
class ViewsController extends AppController {

	private $__words;

	public function beforeFilter() {
		$this->layout = 'view';

		$this->__words = [];
	}
	
	public function index() {
		$m = new MongoCLient();
		$db = $m->facebook;
		$col = $db->posts;

		$cursor = $col->find();
		foreach($cursor as $item) {
			if(!isset($item['message']))
				continue;

			$words = preg_split('/[ ]+/', $item['message']);
			foreach ($words as $key => $value) {
				$v = $value;
				$v = preg_replace('/[\n\']/', '', $v);
				$v = preg_replace('/[.,?:"\'\n]$/', '', $v);

				$words[$key] = $v;		

				if($value == "")
					unset($words[$key]);
			}

			foreach ($words as $key => $value) {
				if (!isset($this->__words[$value]))
					$this->__words[$value] = 1;
				else
					$this->__words[$value]++;
			}
		}

		$this->set('data', $this->__words);
	}	
}