<<<<<<< HEAD
<?php
App::import('Vendor', 'facebook', array('file' => 'facebook-php-sdk-v4-5.0/src/Facebook/autoload.php'));

class HomesController extends AppController {
	public $components = array('Session', 'Cookie');

	const LIMIT = 100;
	
	private $__fb;
	private $__message;
	private $__accessToken;
	private $__collection;
	
	public function beforeFilter() {
		$this->__fb = new Facebook\Facebook(Configure::read('facebook.app'));
		
		$m = new MongoClient();
		$db = $m->facebook;
		$this->__collection = $db->posts;
	}
	
	public function index() {
		$this->autoRender = false;
		$this->layout = false;
		
		$helper = $this->__fb->getRedirectLoginHelper();
		
		$permissions = ['email', 'user_posts', 'user_birthday', 'user_likes', 'user_photos', 'user_location', 'user_status'];
		$callback = Router::url(array('controller' => 'homes', 'action' => 'login'), true);
		$loginUrl = $helper->getLoginUrl($callback, $permissions);
		
		echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
	}
	
	public function login() {
		$this->autoRender = false;
		$this->layout = false;
		
		$helper = $this->__fb->getRedirectLoginHelper();
		
		try {  
			$accessToken = $helper->getAccessToken();  
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			$this->__message = $e->getMessage();
			return;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			$this->__message = $e->getMessage();  
			return;
		}  
		
		if (!isset($accessToken)) {  
		  if ($helper->getError()) {
		  	$this->__message = $helper->getError();
		  	return;
		  } else {
		    $this->__message = 'Bad request';  
		  }  
		  return;
		}  

		//Is ready
		$this->__accessToken = $accessToken->getValue();
		$this->__collectPosts();
		echo "Ok";
	}

	private function __collectPosts($next = null) {
		if($next)
			$url = $next;
		else
			$url = "/me/posts?limit=" . self::LIMIT;

		$res = $this->__fb->get($url, $this->__accessToken)->getDecodedBody();
		
		$this->__savePostsData($res['data']);

		if(isset($res['paging']['next'])) {
			$next = substr($res['paging']['next'], 31);
			unset($res);
			return $this->__collectPosts($next);
		}
	}
	
	private function __savePostsData($data) {
		if(!$data)
			return;

		try {
			$this->__collection->batchInsert($data);
		} catch(Excetption $e) {
			echo $e->getMessage();
		}
	}
}






=======
<?php

class HomesController extends AppController {
	
	public $components = array('Cookie', 'Session');
	
	public function index() {
	}
}
>>>>>>> refs/remotes/origin/master
