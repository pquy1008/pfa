<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset() . PHP_EOL; ?>
	<title> <?php echo $this->fetch('title'); ?></title>
  <script src="/d3.min.js" charset="utf-8"></script>
	<script src="/d3.layout.cloud.js" charset="utf-8"></script>
</head>

<body>
  <div class="content">
    <h2>Word cloud</h2>
    <div id="wordCloud">
    <?php
      echo $this->fetch('content');
    ?>
    </div>
  </div>

  <script>
    var fill = d3.scale.category20();

    d3.layout.cloud().size([1500, 1500])
        .words([
          <?php
            foreach ($data as $key => $value) {
              echo "{'text': '$key', 'size': $value}," . PHP_EOL;
            }
          ?>
          ]
          .map(function(d) {
          return {text: d.text, size: d.size * 10};
        }))
        //.padding(5)
        .rotate(function() { return 0; })
        .font("Arial")
        .fontSize(function(d) { return d.size; })
        .on("end", draw)
        .start();

    function draw(words) {
      d3.select("#wordCloud").append("svg")
          .attr("width", 1500)
          .attr("height", 1500)
        .append("g")
          .attr("transform", "translate(150,150)")
        .selectAll("text")
          .data(words)
        .enter().append("text")
          .style("font-size", function(d) { return d.size + "px"; })
          .style("font-family", "Arial")
          .style("fill", function(d, i) { return fill(i); })
          .attr("text-anchor", "middle")
          .attr("transform", function(d) {
            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
          })
          .text(function(d) { return d.text; });
    }
  </script>
</body>
</html>